var PATH = "Current Settings()"
var TitreOriginal = "File_Name";

# Selection zone de travail
macro "Selection[F1]"
{//setTool("freehand");
 run("Duplicate...", "title=Zone_De_Travail");}

# Filtres
selectWindow(Zone_De_Travail)
macro "Filtrage[F2]"
{run("Smooth");
 run("RGB to CMYK");
 run("Stack to Images");}

# Seuillage Feuille
macro "Feuille[F3]"
{selectWindow("Y");
 run("Duplicate...","title=SeuillageFeuille");
 selectWindow("SeuillageFeuille");
 run("Smooth");
 run("Convert to Mask");
 run("Outline");
 run("Dilate");
 run("Fill Holes");
 run("Create Selection");
 Mesure_EcritureResultat("Feuille");
  }

# Creation Masque Lesion
macro "Lesion[F4]"
{selectWindow("M");
 run("Duplicate...", "title=SeuillageLesion");
 run("Smooth");
 run("Enhance Contrast", "saturated=0.35");
 run("Convert to Mask");
 selectWindow("SeuillageLesion");
 run("Invert LUT");
 }
 
# Creation Masque Feuille
macro "Lesion[F5]"
{selectWindow("SeuillageFeuille");
 run("Create Mask");
 selectWindow("Mask");
 run("Invert LUT");
 }
 
# Seuillage Lesion
macro "Lesion[F6]"
{imageCalculator("AND create 32-bit", "Mask","SeuillageLesion");
 selectWindow("Result of Mask");
 run("Make Binary");
 run("Fill Holes");
 run("Erode"); run("Erode"); run("Erode");
 Mesure_EcritureResultat("Lesion");
 run("Outline");
 imageCalculator("ADD", "Result of Mask","Zone_De_Travail");
 }
 
# Export Resultats
function Mesure_EcritureResultat(cas)
{ run("Analyze Particles...", "size=0.40-Infinity display clear include");
        if (File.exists(PATH+"Resultat.txt")==1) Result = File.openAsString(PATH+"Resultat.txt");
	        else Result ="Number Label Area BX BY Width Height";
	print(Result);
	n=nResults;
     	for (i=0; i<n; i++) { EcritureResultat(i,"cas"); }
            saveAs("Results", PATH+"Resultat.txt");
}

function EcritureResultat(i,cas)
{
  print(TitreOriginal +"  "+cas+"  "+getResult("Area", i),getResult("BX", i),getResult("BY", i),getResult("Width", i),getResult("Height", i));
}
