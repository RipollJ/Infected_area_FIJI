License term are in agreement with Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
See : https://creativecommons.org/licenses/by-nc/4.0/

---------------------------------------------------------------------------------
                   Creative Commons Library Certification

                         CC-CERT-LIB v0.2 Jan 27 2017

This GitHub repository will be the canonical content for structure of the Creative Commons Library certification.

This repository is primarily for the design team to use as the master content. We are developing it Markdown format so it can be easily be deployed to our demonstration public Wordpress site http://certificates.creativecommons.org/certs but potentially other platforms that publish web content form Markdown (see more about our technical approach.

This GitHub repository will be the canonical content for structure of Creative Commons Library certification. Core Modules/units should be edited in cc-cert-map

    Enter the Library Certification
    Library Certification Overview
    Library Content

Learn more about the Creative Commons Certification project at http://certificates.creativecommons.org/ Other related CC certifications are:

    CORE
    Government
    Education

Creative Commons Attribution 4.0 International License Licensed under a Creative Commons Attribution 4.0 International License (CC BY).

Except where otherwise noted, this content is published under a CC BY license, which means that you can copy, redistribute, remix, transform and build upon the content for any purpose even commercially as long as you give appropriate credit and provide a link to the license.

Recommended attribution:

    "Creative Commons Core Certificate" by Creative Commons is licensed under CC BY-NC 4.0 Available at
    https://github.com/creativecommons/cc-cert-lib/blob/master/README.md

Creative Commons Certificates have been created as a project of Creative Commons with the kind support of the Institute of Museum and Library Services and the Bill & Melinda Gates Foundation.

These certificates are part of the commons; if you find broken links or any other errors you can help by reporting them as an issue.

------------------------------------------------------------------------------------------
               Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)

This is a human-readable summary of (and not a substitute for) the license.
You are free to:

    Share � copy and redistribute the material in any medium or format
    Adapt � remix, transform, and build upon the material

    The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

    Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

    NonCommercial � You may not use the material for commercial purposes.

    No additional restrictions � You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Notices:

    You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
    No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.