var PATH = "C:/Users/Documents/"
var TitreOriginal;
var idOriginal, idQualiteSeuilage,idSeuilageFeuille,idSeuilageLesion,idQualiteSeuilageLesion;
var Carac1;
var LongueurCm, LongueurPix;

macro "Ouverture fichier [F1]"
{
      if (isOpen("Log")==1) { selectWindow("Log"); print("\\Clear"); }
       open();
      Dialog.create("D�finition date/Traitement/Plante/Feuille");
      Dialog.addMessage("Caract�risation de l'image");
      Dialog.addString("Carac1:","" );
      Dialog.show();
     Carac1= Dialog.getString(); 
}

macro "Echelle [F2]"
{
// On copie l'image pour travailler dessus
run("Duplicate...","title=Echelle");
idEchelle = getImageID;

// ---- Agrandissement de l'image -----------------------------------------
setJustification("left"); setFont("Monospaced",50,"bold"); setColor(255,0,0);
drawString("+ Agrandissement de l'image n�cessaire ?",getWidth()/300,getHeight()/50);
drawString("     - Zoom Avant sur l'objet   : bouton gauche souris",getWidth()/300,2*getHeight()/50);
drawString("     - Zoom Arri�re sur l'objet : bouton droit souris",getWidth()/300,3*getHeight()/50);
drawString("     - Sortie                   : Alt",getWidth()/300,4*getHeight()/50);
setTool(0);
setTool(11);
flags = -1;
while (flags ==-1 || isKeyDown('alt')!=1)
         { getCursorLoc(x, y, z, flags); wait(10); }

// ---- Selection de l'echelle -----------------------------------------------
drawString("+ Selection de l'echelle",getWidth()/300,6*getHeight()/50);
drawString("        - Cliquez sur 2 points pour d�finir l'echelle (bouton gauche souris)",getWidth()/300,7*getHeight()/50);
leftButton=16;
LongueurCm = 0;
Confirm = 0;
while (Confirm!=1)
   { setTool(0);
     while (LongueurCm==0)
        {  x2=-1; y2=-1; z2=-1; flags2=-1;
            Xdeb=-1; Ydeb=-1; Xfin =-1;
            while (Xfin==-1)
              { setTool(4);
	    getCursorLoc(x, y, z, flags);
                 if (flags!=0 && leftButton!=0 && Xdeb==-1) { Xdeb=x; Ydeb=y; fillRect(Xdeb, Ydeb,3 ,3); }
                 if (flags!=0 && leftButton!=0 && Xdeb!=-1) 
	                {  if (x!=Xdeb && y!=Ydeb) { Xfin=x; Yfin=y;} }
                 if(Xdeb!=-1) makeLine(Xdeb,Ydeb,x,y);
	     wait(50); }
         setLineWidth(5);
         LongueurPix = sqrt( (Xfin-Xdeb)*(Xfin-Xdeb) + (Yfin-Ydeb)*(Yfin-Ydeb));
         Confirm = ConfirmationEchelle(); 
        if (Confirm==0) LongueurCm=0; 
      }
    }
    //close();
    setColor(255,255,255);
    setLineWidth(10);
    drawLine(Xdeb,Ydeb,Xfin,Yfin);
    setLineWidth(5);
    drawLine(Xdeb-40,Ydeb,Xdeb+40,Ydeb);
    drawLine(Xfin-40,Yfin,Xfin+40,Yfin);
    Scale= LongueurPix/LongueurCm;
   selectWindow("Echelle"); close();
// Mise � l'�chelle ---------------------------------
    run("Set Scale...", "distance="+LongueurPix+" known="+LongueurCm+" pixel=1 unit=cm");

     setTool("freehand");

}

function ConfirmationEchelle()    // on tape un chiffre inf�rieur ou �gale � 0 (z�ro) pour la longueur en cm pour recommencer la s�lection de l'echelle
{
	Confirm = 0;
	Dialog.create("D�finition de l'�chelle de l'image");
	Dialog.addMessage("Longueur du segment s�lectionn� :");
	Dialog.addNumber("Longueur [cm] =", 23.16);
	Dialog.addNumber("Longueur [pixel] =", LongueurPix);
	Dialog.show();
	LongueurCm =Dialog.getNumber();
	if (LongueurCm>0) Confirm = 1;
	return Confirm;
}

macro "Seuilage Feuille [F3]"
{
   setBatchMode(true);

   // Mise � l'�chelle ---------------------------------
    Resolution = getResolution();
    run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");

    run("Copy");
    setTool(13);
    if (isOpen("QualiteSeuilage")==0)
	 { run("Internal Clipboard"); rename("ZoneDeTravail");  run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");
   	   run("Internal Clipboard"); rename("ZoneDeTravail_Feuille");  run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");}
    selectWindow("ZoneDeTravail_Feuille");
 
    Confirm=0; Attente = 0;
    Seuil = 60; dSeuil = 5;
    while (Confirm!=1)
     {  setBatchMode(true);
        if (Attente==0)
	{  Attente = 1;
	   if (isOpen("QualiteSeuilage_Feuille")==1) { selectWindow("QualiteSeuilage_Feuille"); close(); }
 	   if (isOpen("ResultatSeuilage_Feuille")==1) { selectWindow("ResultatSeuilage_Feuille"); close(); }
               selectWindow("ZoneDeTravail_Feuille");
 	   SeuilageImage( 0, Seuil-5, Seuil, Seuil+5);
	   selectWindow("ResultatSeuilage"); rename("ResultatSeuilage_Feuille");
  	   run("Invert LUT");
   	   run("Fill Holes");
    	   //selectWindow("ZoneDeTravail_Feuille");
 	   QualiteSeuilage("ZoneDeTravail_Feuille","ResultatSeuilage_Feuille");
	   selectWindow("QualiteSeuilage"); rename("QualiteSeuilage_Feuille");
	  wait(10); }
        setBatchMode("exit and display");
        selectWindow("QualiteSeuilage_Feuille"); 
         if (isKeyDown('shift')==1) { Seuil=Seuil+dSeuil; Attente = 0;}
         if (isKeyDown('space')==1) { Seuil=Seuil-dSeuil; Attente = 0;}
         if (isKeyDown('alt')==1) { Confirm=1; Attente = 0; }
     }
    selectWindow("ResultatSeuilage_Feuille");    run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");
    Mesure_EcritureResultat();

    selectWindow("ZoneDeTravail_Feuille");
    run("Duplicate...", "title=ZoneDeTravail_Lesion");
    selectWindow("ResultatSeuilage_Feuille");
    run("Duplicate...", "title=MasqueFeuille_Lesion");
    run("Invert LUT");
    imageCalculator("AND","ZoneDeTravail_Lesion","MasqueFeuille_Lesion");
    changeValues(000000,000000,0x00ff00);
    selectWindow("MasqueFeuille_Lesion");close();
    selectWindow("ResultatSeuilage_Feuille");close();
    selectWindow("ZoneDeTravail_Feuille");close();
    selectWindow("QualiteSeuilage_Feuille");close();
    selectWindow("ZoneDeTravail"); close();
    selectWindow("ZoneDeTravail_Lesion");
    setBatchMode("exit and display");
}


macro "Seuilage L�sions [F4]"
{
  // Mise � l'�chelle ---------------------------------
    selectWindow("ZoneDeTravail_Lesion");
    Resolution = getResolution();
    run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");
    changeValues(000000,000000,0x00ff00);

    Confirm=0; Attente = 0;
    Seuil = 230; dSeuil = 5;
    while (Confirm!=1)
     {  setBatchMode(true);
        if (Attente==0)
	{  Attente = 1;
	   if (isOpen("QualiteSeuilage_Lesion")==1) { selectWindow("QualiteSeuilage_Lesion"); close(); }
 	   if (isOpen("ResultatSeuilage_Lesion")==1) { selectWindow("ResultatSeuilage_Lesion"); close(); }
               selectWindow("ZoneDeTravail_Lesion");
 	   SeuilageImage( 1, Seuil, Seuil, 0);
	   selectWindow("ResultatSeuilage"); rename("ResultatSeuilage_Lesion");
               run("Fill Holes");
    	   selectWindow("ZoneDeTravail_Lesion");
 	   QualiteSeuilage("ZoneDeTravail_Lesion","ResultatSeuilage_Lesion");
	   selectWindow("QualiteSeuilage"); rename("QualiteSeuilage_Lesion");
	  wait(10); }
        setBatchMode("exit and display");
        selectWindow("QualiteSeuilage_Lesion"); 
         if (isKeyDown('shift')==1) { Seuil=Seuil-dSeuil; Attente = 0;}
         if (isKeyDown('space')==1) { Seuil=Seuil+dSeuil; Attente = 0;}
         if (isKeyDown('alt')==1) { Confirm=1; Attente = 0; }
     }
    selectWindow("ResultatSeuilage_Lesion");    run("Set Scale...", "distance="+Resolution+" known=1 pixel=1 unit=cm");
    Mesure_EcritureResultat();

   // selectWindow("QualiteSeuilage"); close();
    selectWindow("QualiteSeuilage_Lesion"); close();
    selectWindow("ZoneDeTravail_Lesion"); close();
  //  selectWindow("ZoneDeTravail"); close();
    selectWindow("ResultatSeuilage_Lesion"); close();
    close();
    //setBatchMode("exit and display");
}


function SeuilageImage(Substract, Red, Green, Blue)
{
	    // Soustraction du fond --------------------------------
    	    run("Duplicate...", "title=Seuilage_EnCours");
	    idSeuilage_EnCours = getImageID;
	    if (Substract == 1) { run("Subtract Background...", "rolling=1 light sliding"); }

	   // Seuilage de l'image --------------------------------
	    min=newArray(3);
	    max=newArray(3);
	    filter=newArray(3);

	// Seuilage de l'image pour isoler les l�sions ---------------
	  run("RGB Stack");
	  run("Convert Stack to Images");
	  selectWindow("Red"); rename("0");
	  selectWindow("Green"); rename("1");
	  selectWindow("Blue"); rename("2");
	  min[0]=Red; max[0]=255; filter[0]="pass";
	  min[1]=Green; max[1]=255; filter[1]="pass";
	  min[2]=Blue; max[2]=255;filter[2]="pass";
	  for (i=0;i<3;i++) {
	  	selectWindow(""+i);
		setThreshold(min[i], max[i]);
		run("Convert to Mask");
		if (filter[i]=="stop")  run("Invert");}
	  imageCalculator("AND", "0","1");
	  imageCalculator("AND", "0","2");
	  for (i=1;i<3;i++) { selectWindow(""+i); close(); }
	  selectWindow("0");
	  rename("ResultatSeuilage");
	  setForegroundColor(255, 255, 255);
	  run("Erode"); run("Erode");run("Erode");run("Erode");
	  run("Dilate"); run("Dilate");	run("Dilate");run("Dilate");
 }

// Test de la qualit� du seuilage ---------

function QualiteSeuilage(NomImageInitiale, NomImageSeuile)
{
	selectWindow(NomImageSeuile);
            run("Duplicate...", "title=QualiteSeuilage_EnCours");
	run("Outline");
	run("Dilate");
	run("RGB Color");
	changeValues(000000,000000,0xff0000);
            selectWindow(NomImageInitiale);
   	run("Duplicate...", "title=QualiteSeuilage");

	imageCalculator("AND","QualiteSeuilage","QualiteSeuilage_EnCours");
	selectWindow("QualiteSeuilage_EnCours");close();
}

function getResolution()
{
     text = getImageInfo();
   // R�solution de l'image ----------------------------------
     index1= indexOf(text, "Resolution");                             
     index2= indexOf(text, "pixels per cm");
     Resolution = parseFloat(substring(text, index1+11,index2));

   return Resolution;
}

function Mesure_EcritureResultat()
{
 	run("Analyze Particles...", "size=0.01-Infinity circularity=0.00-1.00 show=Nothing display clear include");

            if (isOpen("Log")==1) { selectWindow("Log"); print("\\Clear"); }
            if (File.exists(PATH+"Resultat.txt")==1) Result = File.openAsString(PATH+"Resultat.txt");
	                                                              else Result ="Image NumPlante Objet Surface BX BY Width Height";
	print(Result);
	n=nResults;
     	for (i=0; i<n; i++) { EcritureResultat(i,"Feuille"); }
            saveAs("Text", PATH+"Resultat.txt");
}

function EcritureResultat(i,cas)
{
  print(TitreOriginal +"  "+Carac1+"  "+cas+"  "+getResult("Area", i),getResult("BX", i),getResult("BY", i),getResult("Width", i),getResult("Height", i));
}

